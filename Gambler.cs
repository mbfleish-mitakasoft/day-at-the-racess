﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_at_the_Races__Fixed_
{
    public class Gambler
    {
        public bool isWinner;
        public int WinningLane;
        public int bank, bet, result;
        public int bonus;
        public int LaneChoice;

        public int LaneWon;

        public bool isGameOver;

        public Gambler()
        {
            isWinner = false;
            WinningLane = 0;
            bank = 500; //the starting bank
            bet = 0; //the player's bet
            result = 0; //results in minus the bank or plus the bank
            bonus = 0; //2nd place may pay off, bonus winnings for 1st place

            LaneChoice = 0; //Player chooses which lane will win
            LaneWon = 0; //Which lane actually won

            isGameOver = false;
        }

        public void findWinner(int laneChoice, int laneWon)
        {
            laneChoice = this.LaneChoice;
            laneWon = this.LaneWon;

            if (laneChoice == laneWon)
            {
                switch (laneChoice)
                {
                    case 1:
                        Reward(true);
                        break;
                    case 2:
                        Reward(true);
                        break;
                    case 3:
                        Reward(true);
                        break;
                }
            }
            else
            {
                switch (laneChoice)
                {
                    case 1:
                        Reward(false);
                        break;
                    case 2:
                        Reward(false);
                        break;
                    case 3:
                        Reward(false);
                        break;
                }
            }
        }

        private void Reward(bool isWinner)
        {
            if (!isWinner) //player loses
            {
                bank = bank - bet;
                if (bank <= 0)
                {
                    isGameOver = true;
                }
            }

            else //player wins
            {
                bank = bank + (bet * 2);
            }
        }
    }
}
