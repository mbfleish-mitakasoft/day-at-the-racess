﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Day_at_the_Races__Fixed_
{
    public sealed partial class MainPage : Page
    {
        Gambler gambler = new Gambler();

        Random random = new Random();
        Chicken Chicken1;
        Chicken Chicken2;
        Chicken Chicken3;

        private double ChickenSpeed1, ChickenSpeed2, ChickenSpeed3;

        DispatcherTimer RaceTimer; //Animation Handler
        DispatcherTimer Chicken1Timer;
        DispatcherTimer Chicken2Timer;
        DispatcherTimer Chicken3Timer;

        public MainPage()
        {
            this.InitializeComponent();
            resetButton.Visibility = Visibility.Collapsed;

            RaceTimer = new DispatcherTimer();
            Chicken1Timer = new DispatcherTimer();
            Chicken2Timer = new DispatcherTimer();
            Chicken3Timer = new DispatcherTimer();

            Chicken1 = new Chicken(150, chickenImage1);
            Chicken2 = new Chicken(150, chickenImage2);
            Chicken3 = new Chicken(150, chickenImage3);


            resultText.Visibility = Visibility.Collapsed;


            GoButton.Visibility = Visibility.Collapsed;
            GameOverText.Visibility = Visibility.Collapsed;

            Chicken1Timer.Tick += Chicken1Timer_Tick;
            Chicken2Timer.Tick += Chicken2Timer_Tick;
            Chicken3Timer.Tick += Chicken3Timer_Tick;

            bankText.Text = gambler.bank.ToString();

        }

        
        private void Chicken3Timer_Tick(object sender, object e)
        {
            Chicken3.speed = random.NextDouble();
            chickenImage3.SetValue(Canvas.LeftProperty, Chicken3.StartPOS += Chicken3.speed);
            Chicken3Timer.Interval = TimeSpan.FromMilliseconds(Chicken3.speed);
            Chicken3.ChickenLoc = (double)chickenImage3.GetValue(Canvas.LeftProperty);
            if (Chicken3.ChickenLoc >= 700) StopTime();
        }

        private void Chicken2Timer_Tick(object sender, object e)
        {
            Chicken2.speed = random.NextDouble();
            chickenImage2.SetValue(Canvas.LeftProperty, Chicken2.StartPOS += Chicken2.speed);
            Chicken2Timer.Interval = TimeSpan.FromMilliseconds(Chicken2.speed);
            Chicken2.ChickenLoc = (double)chickenImage1.GetValue(Canvas.LeftProperty);
            Chicken2.DistanceToGo -= 1;
            if (Chicken2.ChickenLoc >= 700) StopTime();
        }

        private void Chicken1Timer_Tick(object sender, object e)
        {
            Chicken1.speed = random.NextDouble();
            chickenImage1.SetValue(Canvas.LeftProperty, Chicken1.StartPOS += Chicken1.speed);
            Chicken1Timer.Interval = TimeSpan.FromMilliseconds(Chicken1.speed);
            Chicken1.ChickenLoc = (double)chickenImage1.GetValue(Canvas.LeftProperty);
            Chicken1.DistanceToGo -= 1;
            if (Chicken1.ChickenLoc >= 700) StopTime();
        }

        private void StopTime()
        {
            Chicken3Timer.Stop();
            Chicken2Timer.Stop();
            Chicken1Timer.Stop();
            GetWinner();
        }

        private void GetWinner()
        {
            if (Chicken1.ChickenLoc >= 700)
            {
                Lane1Text.Text = "CHICKEN 1 WINS!!";
                gambler.LaneWon = 1;
            }
            if (Chicken2.ChickenLoc >= 700)
            {
                Lane2Text.Text = "CHICKEN 2 WINS!!";
                gambler.LaneWon = 2;
            }
            if (Chicken3.ChickenLoc >= 700)
            {
                Lane3Text.Text = "CHICKEN 3 WINS!!";
                gambler.LaneWon = 3;
            }

            //Sets the isWinner bool if the player won or not
            if (gambler.LaneWon == gambler.LaneChoice) gambler.isWinner = true;
            else gambler.isWinner = false;

            //Executes the math
            gambler.findWinner(gambler.LaneChoice, gambler.LaneWon);
            UpdateLabels();


            resetButton.Visibility = Visibility.Visible;
        }

        private void UpdateLabels()
        {
            if (gambler.isWinner)
            {
                resultText.Text = "You Win!";
                resultText.Visibility = Visibility.Visible;
            }
            else
            {
                resultText.Text = "You Lose!";
                resultText.Visibility = Visibility.Visible;
            }

            bankText.Text = gambler.bank.ToString();

            if (gambler.isGameOver)
                GameOverText.Visibility = Visibility.Visible;
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            resetButton.Visibility = Visibility.Collapsed;
            resultText.Visibility = Visibility.Collapsed;

            chickenImage1.SetValue(Canvas.LeftProperty, 150);
            Chicken1.StartPOS = 150;
            Lane1Text.Text = "Lane 1";

            chickenImage2.SetValue(Canvas.LeftProperty, 150);
            Chicken2.StartPOS = 150;
            Lane2Text.Text = "Lane 2";

            chickenImage3.SetValue(Canvas.LeftProperty, 150);
            Chicken3.StartPOS = 150;
            Lane3Text.Text = "Lane 3";

            registerButton.Visibility = Visibility.Visible;

            if (gambler.isGameOver)
            {
                GameOverText.Visibility = Visibility.Collapsed;
                gambler.bank = 500;
                gambler.bet = 0;
                bankText.Text = gambler.bank.ToString();
                betText.Text = gambler.bet.ToString();
                gambler.isGameOver = false;
            }
        }

        private async void registerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                gambler.bet = Convert.ToInt32(betBox.Text);
                if (gambler.bet <= gambler.bank)
                {
                    betText.Text = gambler.bet.ToString();
                }
            }
            catch
            {
                MessageDialog msg = new MessageDialog("Invalid entry.  Enter a number", "Gambling Error");
                await msg.ShowAsync();
            }

            try
            {
                gambler.LaneChoice = Convert.ToInt32(laneBox.Text);
                if (gambler.LaneChoice == 1 || gambler.LaneChoice == 2 || gambler.LaneChoice == 3)
                {
                    laneText.Text = gambler.LaneChoice.ToString();
                    GoButton.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageDialog laneBetError = new MessageDialog("Please enter 1, 2 or 3 in the second box.  It cannot be blank or contain letters",
                        "Gambling Error");
                    await laneBetError.ShowAsync();
                }
            }
            catch
            {
                MessageDialog FormatError = new MessageDialog("There was a problem processing your bet.  Make sure your formatting is correct.",
                    "Gambling Error");
                await FormatError.ShowAsync();
            }

        }


        private void GoButton_Click(object sender, RoutedEventArgs e)
        {
            registerButton.Visibility = Visibility.Collapsed;
            GoButton.Visibility = Visibility.Collapsed;
            Chicken1Timer.Start();
            Chicken2Timer.Start();
            Chicken3Timer.Start();
        }
    }
}
